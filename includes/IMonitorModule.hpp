
#ifndef IMONITORMODULE_HPP
#define IMONITORMODULE_HPP

#include <string>

class	IMonitorModule
{
protected:
	std::string	_name;
	std::string	_postfix;
public:
	IMonitorModule(std::string name) : _name(name) {}
	IMonitorModule(IMonitorModule const &obj) { *this = obj; }
	virtual ~IMonitorModule(void) {}
	
	virtual void			setPostfix(std::string postfix) = 0;

	virtual std::string		getPostfix(void) const = 0;
	virtual std::string		getName(void) const = 0;
};

#endif
