#ifndef DISPLAY_HPP
#define DISPLAY_HPP

#include "IMonitorDisplay.hpp"
#include "Module.hpp"
#include <vector>
#include <iostream>

#include <iostream>
#include <list>
#include <ncurses.h>
#include <fstream>

namespace	cs
{
const char	*s[12] = {
		"hostname",
		"sw_vers | cut -f2 | tr \"\n\" \" \"",
		"top -l1 -n0 | sed -n 2p | cut -d\" \" -f1",
		"top -l1 -n0 | sed -n 2p | cut -d\" \" -f2",
		"sysctl -n machdep.cpu.brand_string",
		"sysctl -n hw.cpufrequency",
		"top -l1 -n0 | sed -n 4p | cut -d\" \" -f3",
		"top -l1 -n0 | sed -n 4p | cut -d\" \" -f5",
		"top -l1 -n0 | sed -n 4p | cut -d\" \" -f7",
		"sysctl -n machdep.cpu.core_count",
		"top -l1 -n0 | sed -n 7p | cut -d\" \" -f2",
		"top -l1 -n0 | grep Network | cut -d\" \" -f2 -f3 -f4 -f5 -f6"
};

int		programLoop = 1;
int		width;
int		height;
}

void		controls(int keycode)
{
	if (keycode == 27)
		cs::programLoop = 0;
}

std::string	extract_cmd(std::string command)
{
	redi::ipstream in(command, redi::pstreams::pstdout | redi::pstreams::pstdin);
	std::string line;
	std::getline(in.out(), line);
	return (line);
}

class IMonitorDisplay;

class Display : public IMonitorDisplay
{
protected:
	std::vector <Module>	v;
public:
	Display(void) : IMonitorDisplay()
	{
		this->v.reserve(12);
		v.push_back(Module("Hostname"));
		v.push_back(Module("OS info"));
		v.push_back(Module("Date"));
		v.push_back(Module("Time"));
		v.push_back(Module("CPU model"));
		v.push_back(Module("CPU lock speed"));
		v.push_back(Module("CPU user activity"));
		v.push_back(Module("CPU system activity"));
		v.push_back(Module("CPU idle"));
		v.push_back(Module("CPU number of cores"));
		v.push_back(Module("RAM"));
		v.push_back(Module("Network"));
	}
	Display(Display const& obj) { *this = obj; }
	virtual ~Display(void) {}

	Display	&operator =(Display const &obj) { v = obj.v; return *this; }

	void	gatherData(void)
	{
		std::vector <std::string> 				s(cs::s, std::end(cs::s));
		std::vector <std::string> ::iterator	it_s = s.begin();//i2->setPostfix(extract_cmd(*i1));
		std::vector <Module> ::iterator			it_v;

		for (it_v = v.begin(); it_v < v.end(); it_v++)
		{
			it_v->setPostfix(extract_cmd(*it_s));
			it_s++;
		}
	}

	void	displayData(void)
	{
		initscr();
		noecho();
		curs_set(0);
		nodelay(stdscr, true);
		getmaxyx(stdscr, cs::height, cs::width);

		while (cs::programLoop)
		{
			gatherData();
			refresh();
			clear();
			std::vector <Module> ::iterator		it_v;
			int									y = 0;
			std::ofstream file("data.txt");
			for (it_v = v.begin(); it_v < v.end(); it_v++)
			{
				mvprintw((y += 2), 10, "%s :  %s", it_v->getName().c_str(), it_v->getPostfix().c_str());
				file << it_v->getName() << "=" << it_v->getPostfix() << std::endl;
			}
			file.close();
			controls(getch());
		}
		std::vector <Module> ::iterator			it_v;
	}
};

#endif