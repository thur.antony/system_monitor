#ifndef MODULE_HPP
# define MODULE_HPP

#include "IMonitorModule.hpp"

class IMonitorModule;

class Module : public IMonitorModule
{
public:
	Module(void);
	Module(std::string name) : IMonitorModule(name) {}
	Module(Module const& obj) : IMonitorModule(obj) {}
	~Module(void) {}

	Module	&operator =(Module const &obj) { _name = obj._name; _postfix = obj._postfix; return *this; }

	void	setPostfix(std::string postfix)
	{
		_postfix = postfix;
	}

	std::string	getPostfix(void) const
	{
		return _postfix;
	}

	std::string getName(void) const
	{
		return _name;
	}
};
#endif
