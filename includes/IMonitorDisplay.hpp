
#ifndef IMONITORDISPLAY_HPP
#define IMONITORDISPLAY_HPP

#include "pstream.hpp"

class	IMonitorDisplay
{

public:
	IMonitorDisplay(void) {}
	IMonitorDisplay(IMonitorDisplay const &obj) { *this = obj; }
	virtual ~IMonitorDisplay(void) {}

	virtual void	gatherData(void) = 0;

	virtual void	displayData(void) = 0;

};

#endif