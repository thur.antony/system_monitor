NAME = ft_gkrellm

SRC_PROJECT = ./srcs/main.cpp

OBJECT_PROJECT = $(SRC_PROJECT:.cpp=.o)
FLAGS = -O3 -Wall -Wextra -Werror -lcurses
INCLUDES = -Iincludes

all: $(NAME)

$(NAME): #$(OBJECT_PROJECT)
	@clang++ $(INCLUDES) $(SRC_PROJECT) -o $(NAME) $(FLAGS)

%.o: %.c
	@gcc $(FLAGS) -o $@ -c $<

clean:
	@/bin/rm -f $(OBJECT_PROJECT)

fclean: clean
	@/bin/rm -f $(NAME)

re:	fclean all
